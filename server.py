import socket
import threading
import sys
!pip install pyodbc
import pyodbc

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
MAC = '08:00:27:65:a7:5f\n'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
flag = 0 

server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True

    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            
            global flag
            if flag == 0:
                if msg == MAC:
                    print("Usuario registrado, se acepta el envio de data")
                    flag = 1
                if msg != MAC:
                    print("Usuario no registrado, no tiene permiso para enviar data")
                    break

            if msg == DISCONNECT_MESSAGE:
                connected = False
            print(f"[{addr}] {msg}")
            conn.send("Msg received".encode(FORMAT))

    temp=msg[1]
    hum=msg[2]
    presion=msg[3]
    Server = '158.251.91.43,5050;' 
    database = 'riego;' 
    username = 'sa;' 
    password = 'Admin.123;' 
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+Server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute('exec SP_writeArduino ?', 'UNO')
    cursor.execute('exec SP_writeTemperatura ?',temp)
    cursor.execute('exec SP_writeHum ?',hum)
    cursor.execute('exec SP_writePresion ?',presion)
    cursor.commit()

    conn.close()

def start():

    try:    
        server.listen()
    except (KeyboardInterrupt, SystemExit):
        conn.close()
        sys.exit(2)

    except Exception as e:
        print(e)
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")

start()


