import socket
import numpy as np
import time

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
flag = 0

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):
    message=msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))

def data():
    temp=float(np.random.normal(20,5,1))
    presion=float(np.random.normal(20,5,1))
    humedad=float(np.random.normal(50,30,1))
    datos=[temp,humedad,presion]
    return datos


while True:
    if flag == 0:
        mensaje = open("/sys/class/net/enp0s3/address","r").read()
        send(str(mensaje))
        flag = 1
    print("Se comenzara a enviar data aleatoria")
    for i in range (1,10):
        dts=data()
        dts=str(data)
        send(dts)
        time.sleep(10)
    send(DISCONNECT_MESSAGE)
    break
